#Python 3.7.9
import socket
import re, glob, os, threading
import wave, pyaudio, pickle, struct, sys

ip = '127.0.0.1'
port = 9999

def getOutputFile():
	path = './output/'
	file = 'output'
	try:
		# get last number + 1  of output file
		num = sorted([int(num) for file in glob.glob(path + file + '*.wav') for num in re.findall("(\d{1,10})", file)], reverse=True)[0] + 1
	except:
		if not os.path.isdir(path):
			os.mkdir(path)
		num = 1
		#TODO Check if path is writable
	return path + file + str(num) + '.wav'

# set output wave file format and return file
def getOutput(file):
	try:
		waveFile = wave.open(file, 'wb')
	except:
		print("Can't create file:", file)
		sys.exit()
	p = pyaudio.PyAudio()
	waveFile.setsampwidth(p.get_sample_size(2))
	waveFile.setnchannels(2)
	waveFile.setframerate(22050 / 2)
	return waveFile

def ClientSocketHandler(c, ip, outputFile):
	data = b""
	payLoadSize = struct.calcsize("Q")
	waveFile = getOutput(outputFile)
	while True:
		try:
			while len(data) < payLoadSize:
				packet = c.recv(4 * 1024)
				if not packet:break
				data += packet
			packedMsgSize = data[:payLoadSize]
			data = data[payLoadSize:]
			msg_size = struct.unpack("Q", packedMsgSize)[0]
			while len(data) < msg_size:
				data += c.recv(4 * 1024)
			frameData = data[:msg_size]
			data = data[msg_size:]
			frame = pickle.loads(frameData)
			waveFile.writeframes(frame)
		except Exception as e:
			break
	waveFile.close()
	c.close()
	print('Audio stream saved to:', outputFile)

def main(ip, port):
	serverSocket = socket.socket()
	serverSocket.bind((ip, port))
	serverSocket.listen(5)
	print("Server listening at:", ip + ':' + str(port))
	while True:
		outputFile = getOutputFile()
		c, addr = serverSocket.accept()
		#threading is not so fast, but working
		threading._start_new_thread(ClientSocketHandler, (c, addr, outputFile))

if __name__ == "__main__":
	main(ip, port)