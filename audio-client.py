#Python 3.7.9
import socket
import wave, pickle, struct

def audioStreamSend(file, ip, port):
	buffer = 1024
	try:
		waveFile = wave.open(file, 'rb')
	except OSError as e:
		print(e)
		exit()
	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socketAddress = (ip, port)
	try:
		clientSocket.connect(socketAddress)
	except socket.error as e:
		print(e)
		exit()

	print("Client connected to:", socketAddress[0] + ':' + str(socketAddress[1]))
	if clientSocket:
		while True:
			data = waveFile.readframes(buffer)
			if len(data) == 0:
				print("Audio file was send.")
				break
			a = pickle.dumps(data)
			message = struct.pack("Q", len(a)) + a
			clientSocket.sendall(message)

audioStreamSend("Ring05.wav", '127.0.0.1', 9999)
